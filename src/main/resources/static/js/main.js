$(document).ready(function () {

    $("#search-form").submit(function (event) {

        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var term = $("#term").val();

    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "search",
        data: {
            term: encodeURIComponent(term)
        },
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>Количество повторений</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#feedback').html(json);

            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Произошла ошибка</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            $("#btn-search").prop("disabled", false);

        }
    });

}