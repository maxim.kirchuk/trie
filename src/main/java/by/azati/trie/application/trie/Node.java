package by.azati.trie.application.trie;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс, описывающий узел префиксного дерева {@link Trie}
 *
 * @author M. Kirchuk
 * @since 28 март 2022 г.
 */
 public class Node {

    /**
     * Дочерние узлы. Для листа <tt>#isEmpty() == true</tt>
     */
    private final Map<Character, Node> children;

    /**
     * Маркер, указывающий на то, является ли данный узел завершением строки
     */
    private boolean isEndOfString = false;

    /**
     * Количество повторений слова в тексте
     */
    @Getter
    private int count = 0;

    /**
     * Конструктор узла префиксного дерева {@link Trie}
     *
     * @param string строка, добавляемая в префиксное дерево {@link Trie}
     */
    private Node(String string) {
        children = new HashMap<>();

        if (string.isEmpty()) {
            isEndOfString = true;
            count++;
        } else {
            add(string);
        }
    }

    /**
     * Создает корневой узел префиксного дерева {@link Trie}
     *
     * @return корневой узел префиксного дерева {@link Trie}
     */
    static Node rootNode() {
        return new Node("");
    }

    /**
     * Добавляет данную строку в виде дочерних узлов данного узла в префиксное дерево {@link Trie}
     *
     * @param string добавляемая в префиксное дерево строка {@link Trie}
     * @return <tt>true</tt>, если префиксное дерево {@link Trie} не содержало данную строку ранее
     */
    public boolean add(String string) {
        if (string.isEmpty()) {
            boolean contained = isEndOfString;
            isEndOfString = true;
            count++;

            return !contained;
        }

        char nextKey = string.charAt(0);

        if (children.containsKey(nextKey)) {
            return children.get(nextKey).add(string.substring(1));
        }

        children.put(nextKey, new Node(string.substring(1)));
        return true;
    }


    /**
     * Осуществляет поиск дочернего узла префиксного дерева {@link Trie} по данному префиксу
     *
     * @param prefix префикс, по которому осуществляется поиск узла
     * @return узел префиксного дерева {@link Trie} по данному префиксу или <tt>null</tt>, если такой префикс в дереве не содержится
     */
    public Node findChildByPrefix(String prefix) {
        if (prefix.isEmpty())
            return this;

        char nextKey = prefix.charAt(0);

        if (!children.containsKey(nextKey))
            return null;

        return children.get(nextKey).findChildByPrefix(prefix.substring(1));
    }

    /**
     * Возвращает количество слов в тексте начиная от узла
     *
     * @param root узел с которого начать поиск
     * @return Возвращает количество {@link Integer} уникальных слов в тексте
     */
    public Integer wordCount(Node root)
    {
        int result = 0;

        // определяем или это слово
        if (root.isEndOfString)
            result += root.count;

        for (Map.Entry<Character, Node> entry : root.children.entrySet()) {
            Node value = entry.getValue();
            result += wordCount(value);
        }

        return result;
    }
}
