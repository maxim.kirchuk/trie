package by.azati.trie.application.trie;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;

/**
 * Класс, описывающий префиксное дерево
 *
 * @author M. Kirchuk
 * @since 28 март 2022 г.
 */
@Slf4j
public class Trie {
    private final Node root;

    public Trie() {
        this.root = Node.rootNode();
    }

    /**
     * Добавляет строку в это дерево
     *
     * @param string добавляемая строка в дерево
     * @return <tt>true</tt>, если дерево не содержало данную строку ранее
     */
    public boolean add(String string) {
        if (string.isEmpty())
            return false;

        return root.add(string);
    }

    /**
     * Возвращает количество повторенний заданного слова в тексте
     *
     * @param prefix префикс поиска строк в дереве
     * @return количество {@link Integer} повторенний заданного слова в тексте
     */
    public Integer count(String prefix) {
        Node prefixedNode = getNodeByPrefix(prefix);
        if (prefixedNode != null) {
            return prefixedNode.getCount();
        } else {
            log.info(String.format("%s not found", prefix));
            return 0;
        }
    }

    /**
     * Возвращает количество всех уникальных слов в тексте
     * - 1 так как при инициализации дерева root элемент ""
     *
     * @return Возвращает количество {@link Integer} уникальных слов в тексте
     */
    public Integer countAll() {
        return root.wordCount(root) - 1;
    }

    /**
     * Возвращает {@link Node} или null по данному префиксу
     *
     * @param prefix префикс, которому должен соответствовать искомый {@link Node}
     * @return {@link Node}, если таковой существует по искомому префиксу, в противном случае <tt>null</tt>
     */
    @Nullable
    private Node getNodeByPrefix(String prefix) {
        return root.findChildByPrefix(prefix);
    }
}
