package by.azati.trie.application.service;

import by.azati.trie.application.trie.Trie;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * Сервис для загрузки слов из файла в Trie(префиксное дерево)
 *
 * @author M. Kirchuk
 * @since 28 март 2022 г.
 */
@Service
@Slf4j
public class TrieService {

    @Getter
    private Trie trie;

    @Autowired
    public TrieService(
            @Value("${book.file.path}") Resource resourceFile
    ) {
        trie = loadWords(resourceFile);
    }

    private Trie loadWords(Resource resource) {
        Trie trie = new Trie();

        try {
            Scanner s = new Scanner(resource.getInputStream());

            while (s.hasNext()) {
                trie.add(s.next().replaceAll("[^A-Za-zА-Яа-я0-9'Ёё]", ""));
            }
        } catch (IOException e) {
            log.error("Error accessing input file!");
        }

        return trie;
    }

    public Integer countWordInText(String word) throws UnsupportedEncodingException {
        return trie.count(word);
    }

    public Integer countAllWordInText() {
        return trie.countAll();
    }
}
