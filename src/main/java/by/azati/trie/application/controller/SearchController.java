package by.azati.trie.application.controller;

import by.azati.trie.application.service.TrieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * API получения информации о количестве слов в книге
 * (ищем точное соответствие, а не вхождения – при поиске слова cat не учитываем catalog)
 * REST api
 *
 * @author M. Kirchuk
 * @since 28 март 2022 г.
 */
@RestController
@Slf4j
@Tag(name = "API",description = "API получения информации о количестве слов в книге")
public class SearchController {

    @Autowired
    TrieService service;

    @GetMapping("search")
    @Operation(summary = "Возвращает количество повторенний заданного слова в тексте")
    public ResponseEntity<Integer> search(
            @Parameter(description = "Строка для поиска по тексту") @RequestParam(name = "term", required = false) String term) {
        if (StringUtils.isNotEmpty(term)) {
            try {
                var word = URLDecoder.decode(term, "UTF-8" );
                var countWordInText = service.countWordInText(word);
                return new ResponseEntity<>(countWordInText, HttpStatus.OK);
            } catch (UnsupportedEncodingException e) {
                return new ResponseEntity<>(0, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        return new ResponseEntity<>(0, HttpStatus.OK);
    }

    @GetMapping("countAll")
    @Operation(summary = "Возвращает количество всех уникальных слов в тексте")
    public ResponseEntity<Integer> countAll() {
        var countAllWordInText = service.countAllWordInText();
        return new ResponseEntity<>(countAllWordInText, HttpStatus.OK);
    }
}
