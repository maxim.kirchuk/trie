package by.azati.trie.application.controller;

import by.azati.trie.application.service.TrieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Контроллер для работы формы на Thymeleaf
 *
 * @author M. Kirchuk
 * @since 28 март 2022 г.
 */
@Controller
public class UIController {

    @Autowired
    TrieService service;

    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String homePage(Model model) {
        var countAllWordInText = service.countAllWordInText();
        model.addAttribute("countAll", countAllWordInText);
        return "home";
    }
}
