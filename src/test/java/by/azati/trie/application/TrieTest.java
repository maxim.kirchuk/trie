package by.azati.trie.application;

import by.azati.trie.application.trie.Trie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TrieTest {

    private Trie trie;

    @Before
    public void init() {
        trie = new Trie();
    }

    @Test
    public void add() {
        assertFalse(trie.add(""));

        assertTrue(trie.add("hello"));
        assertFalse(trie.add("hello"));
    }

    @Test
    public void count() {
        assertEquals(Integer.valueOf(0), trie.count("hello"));
        trie.add("hello");
        trie.add("hello");
        assertEquals(Integer.valueOf(2), trie.count("hello"));
        trie.add("world");
        assertEquals(Integer.valueOf(1), trie.count("world"));
    }

    @Test
    public void countAll() {
        assertEquals(Integer.valueOf(0), trie.countAll());
        trie.add("hello");
        trie.add("world");
        trie.add("trie");
        trie.add("hello");
        assertEquals(Integer.valueOf(4), trie.countAll());
    }
}

