Файл с текстом ../Trie/src/main/resources/words.txt

Запуск из Intellij IDEA как Spring Boot Application
или java -jar application-0.0.1-SNAPSHOT.jar 
jar файл в каталоге target

Реализована UI, доступна по адресу http://localhost:8080/

Реализовано 2 GET REST сервиса:
http://localhost:8080/countAll - возвращает общее количество слов тексте
http://localhost:8080/search?term=java - возвращает количество вхождений конкретного слова в тест

Swagger UI
http://localhost:8080/swagger-ui/index.html

Методы реализованные для префиксного дерева:
добавление строки в дерево
возврат количества всех уникальных слов в тексте
поиск количества вхождения определенного слова в тест